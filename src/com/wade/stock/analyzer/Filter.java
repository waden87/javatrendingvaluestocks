package com.wade.stock.analyzer;

import java.util.LinkedList;

import com.wade.stock.common.Stock;

/**
 * Relative strength over 6 months It is calculated dividing the price change of
 * a stock by the price change of the index for the same time period. e.g. A
 * stock falling by 20% versus an index rising 20% would lead to a Relative
 * strength calculation of 100 * ( 80/120 - 1) = -33%
 **/
public class Filter {

	/**
	 * S&P median P/E is 15
	 **/
	public static final float PE_LIMIT = 30.0f;
	public static final float PS_LIMIT = 30.0f;
	public static final float PB_LIMIT = 30.0f;
	public static final float PFCF_LIMIT = 20.0f;
	/* avoid stocks that have dropped significantly in value but had high yields */
	public static final float SHAREHOLDER_YIELD_LIMIT = 30.0f;
	/**
	 * arbitrary limit
	 */
	public static final double MARKET_CAP_LIMIT = 200000000.0f; // 200 million

	/**
	 * returns true if the indicated stock passes a set of tests
	 * 
	 * @param stock
	 * @return
	 */
	public LinkedList<Stock> applyFilterTest(LinkedList<Stock> stockList) {
		LinkedList<Stock> newList = new LinkedList<Stock>();

		for (Stock current : stockList) {
			if (current.marketCap > MARKET_CAP_LIMIT) {
				newList.add(current);
			}
		}

		return newList;
	}

	/**
	 * applies a ranking value (1-100) for certain parameters
	 * 
	 * @param stocks
	 */
	public void applyRanks(LinkedList<Stock> stocks) {
		double pe, ps, pb, ev, fcf, sh;
		double totalRank = 0;

		System.out.println("Max Price over Earnings is:"
				+ Stock.maxPriceOverEarnings);
		System.out
				.println("Max Price over Sales is:" + Stock.maxPriceOverSales);

		System.out.println("Max Price over Booking is:"
				+ Stock.maxPriceOverBook);

		System.out.println("Max Shareholder Yield is:"
				+ Stock.maxShareholderYield);
		
		System.out.println("Max Price Over free cash flow:"
				+ Stock.maxPriceOverFCF+ " symbol:"+Stock.maxPriceOverFCFSymbol);

		System.out.println(Stock.maxShareholderYieldSymbol
				+ " has the highest yield");

		for (Stock entry : stocks) {
			// apply price over earnings rank
			pe = entry.getPE();
			if (pe > 0) {
				entry.setPERank(((Stock.maxPriceOverEarnings - pe) / Stock.maxPriceOverEarnings) * 100);
			}
			// apply price over sales rank
			ps = entry.getPS();
			if (ps > 0) {
				entry.setPSRank(((Stock.maxPriceOverSales - ps) / Stock.maxPriceOverSales) * 100);
			}
			// apply price over booking rank
			pb = entry.getPB();
			if (pb > 0) {
				entry.setPBRank(((Stock.maxPriceOverBook - pb) / Stock.maxPriceOverBook) * 100);
			}
			// apply price over Earned vvalue rank
			ev = entry.getEV();
			if (ev > 0) {
				entry.setEVRank(((Stock.maxEvOverEbitda - ev) / Stock.maxEvOverEbitda) * 100);
			}
			// apply price over free cash flow rank
			fcf = entry.getPFCF();
			if (fcf > 0) {
				entry.setPFCFRank(((Stock.maxPriceOverFCF - fcf) / Stock.maxPriceOverFCF) * 100);
			}
			// apply shareholder yield rank
			sh = entry.getShareholderYield();
			if (sh > 0) {
				entry.setShareholderYieldRank(((/*Stock.maxShareholderYield -*/ sh) / Stock.maxShareholderYield) * 100);
				/*
				 * System.out .println("shareholder yield rank:" +
				 * entry.getShareholderYieldRank() + " Max yield:" +
				 * Stock.maxShareholderYield + " ShareholderYield:" + sh);
				 */
			}
			totalRank += entry.getPERank();
			totalRank += entry.getPSRank();
			totalRank += entry.getPBRank();
			totalRank += entry.getEVRank();
			totalRank += entry.getPFCFRank();
			totalRank += entry.getShareholderYieldRank();

			// combine the ranks to determine a score (600 = perfect)
			entry.setTotalRank(totalRank);
			totalRank = 0;// reset for next stock
		}
	}
}
