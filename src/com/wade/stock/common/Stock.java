package com.wade.stock.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.wade.stock.analyzer.Filter;

public class Stock {
	/**
	 * valid set of dates for which data has been obtained
	 */
	public static Date[] dates;
	/**
	 * first day with to start analysis
	 */
	public static Date startDate;
	/**
	 * last day to end analysis
	 */
	public static Date stopDate;
	/**
	 * date that is six months before the stop date, used to calculate relative
	 * strength
	 */
	public static Date sixMonthsFromStop;
	/**
	 * number of actual active trading days
	 */
	public static int numValidTradingDays;
	/**
	 * flag states if the stored data should be used
	 */
	public static boolean useStoredDate = true;

	/**
	 * P/E (price over earnings ratio) r-yahoo
	 */
	public double priceOverEarnings;
	public static double maxPriceOverEarnings = 0.0f;
	public double priceOverEarningsRank;

	/**
	 * P/S (Price over sales ratio) p5-yahoo
	 */
	public double priceOverSales;
	public static double maxPriceOverSales = 0.0f;
	public double priceOverSalesRank;

	/** market capitalization **/
	public double marketCap;
	/** market capitalization real time **/
	public double marketCapRealTime;

	/**
	 * indicates stock has passed initial tests
	 */
	public boolean valid;

	/**
	 * P/B (Price to book ratio) stockPrice/ (total assets - intangible assets
	 * and liabilities) p6-yahoo
	 */
	public double priceOverBook;
	public static double maxPriceOverBook = 0.0f;
	public double priceOverBookRank;

	/** string representing the sector the stock is involved in **/
	public String sector;
	/**
	 * P/FCF (Price over free cash flow)
	 */
	// public float priceOverFreeCashFlow;

	/**
	 * shareholder yield (Total amount paid in dividends + Total amount paid in
	 * share repurchases - value of shares issued) = (total spent on dividends +
	 * net share repurchase), divide this by market capitalization ->
	 * shareholder yield
	 */
	// public float shareholderYield;

	/**
	 * EV/EVBITDA - Enterprise value over (Earnings before interest, taxes
	 * depriciation and amortization) -yahoo-key-statistics
	 */
	public double evOverEbitda;
	public static double maxEvOverEbitda;
	public double evOverEbitdaRank;

	/**
	 * Price over Free Cash flow = (Market Capitolization)/ (Free cash flow)
	 */
	public double priceOverFCF;
	public static double maxPriceOverFCF = 0.0;
	public static String maxPriceOverFCFSymbol = null;
	public double priceOverFCFRank;

	/**
	 * shareholder yield. calculated as (dividends + stock buy back - stock
	 * sales) / market capitalization
	 */
	public double shareholderYield;
	public static double maxShareholderYield = 0.0;
	public static String maxShareholderYieldSymbol = null;
	public double shareholderYieldRank;

	public double totalRank;

	public float[] dailyPrices;
	public String stockSymbol;

	public double sixMonthChange;

	/*
	 * public GatherDataProcess processThread;
	 * 
	 * static private ThreadGroup threadGroup = new ThreadGroup("Stock Gather");
	 * 
	 * private class GatherDataProcess implements Runnable {
	 * 
	 * Stock stock;
	 * 
	 * public GatherDataProcess(Stock stock) { this.stock = stock; }
	 * 
	 * @Override public void run() {
	 * 
	 * }
	 * 
	 * }
	 */

	public String getSymbol() {
		return stockSymbol;
	}

	public Stock(String symbol, boolean getFreshCopy, String sector) {
		dailyPrices = new float[numValidTradingDays];
		for (int i = 0; i < numValidTradingDays; i++) {
			dailyPrices[i] = -999.0f;
		}
		this.sector = sector;

		this.stockSymbol = symbol;

		// set initial values to invalid

		this.priceOverEarnings = -9999.0f;
		this.priceOverSales = -9999.0f;
		this.priceOverBook = -9999.0f;
		this.marketCap = -9999.0f;
		this.marketCapRealTime = -9999.0f;
		this.priceOverEarningsRank = 0;
		this.priceOverSalesRank = 0;
		this.priceOverBookRank = 0;
		this.evOverEbitda = 0;
		this.evOverEbitdaRank = 0;
		this.priceOverFCF = 0;
		this.priceOverFCFRank = 0;
		this.totalRank = 0;
		this.sixMonthChange = 0;
		this.valid = false;

		// long start, stop;
		// total;
		// start = System.currentTimeMillis();
		List<String[]> dataEntries;
		String[] keyStats = null;
		String[] miscStats = null;

		if (getFreshCopy == false) { // utilized stored data
			dataEntries = StockUtility.getStoredStockEntry(symbol);
			if (dataEntries != null) {
				keyStats = StockUtility.getStoredKeyStats(symbol);
			}
		} else { // grab historical data from the internet
			dataEntries = StockUtility.getStockEntries(StockUtility
					.generateStockUrl(startDate, stopDate, symbol));
			// be zero to not totally throw out a stock just because the data
			// cannot be found.
			if (dataEntries != null) {
				try {
					Thread.sleep(20L);// sleep to prevent DDOS-ing yahoo finance
				} catch (InterruptedException e) {
					System.out
							.println("ERROR: trying to sleep on key statistics "
									+ e.toString());
				}
				/*
				 * get key statistics this includes P/E, P/Sales, P/Booking,
				 * market capitalization, market cap realtime
				 */
				String[] partStats = StockUtility.getKeyStatistics(symbol);

				try {
					Thread.sleep(20L);// sleep to prevent DDOS-ing yahoo finance
				} catch (InterruptedException e) {
					System.out
							.println("ERROR: trying to sleep on Misc statistics "
									+ e.toString());
				}

				miscStats = StockUtility.getMiscStats(symbol);

				if (miscStats == null) {
					System.out.println("Error retrieving misc stats for:"
							+ symbol);
					return;
				}
				// combine arrays of key statistics
				keyStats = ArrayUtils.addAll(partStats, miscStats);
			} else {
				System.out.println("cannot get data for (" + symbol + ")");
			}
		}

		if (dataEntries == null || keyStats == null) {
			this.stockSymbol = null;
			return;
		} else if (getFreshCopy == true) {
			// store the fresh copy to the hard-drive
			// reverse the list to be in ascending order by date
			Collections.reverse(dataEntries);
			StockUtility.storeData(symbol, dataEntries);
			StockUtility.storeKeyStats(symbol, keyStats);
		}

		if (keyStats.length < 4) {
			return;
		}

		if (keyStats != null) {// copy in correct data;
			try {
				this.priceOverEarnings = Double
						.parseDouble(keyStats[StockUtility.KEY_STAT_PE_RATIO]);
			} catch (NumberFormatException e) {
				this.priceOverEarnings = -9999.0f;
			}

			try {
				this.priceOverSales = Double
						.parseDouble(keyStats[StockUtility.KEY_STAT_PS_RATIO]);
			} catch (NumberFormatException e) {
				this.priceOverSales = -9999.0f;
			}
			try {
				this.priceOverBook = Double
						.parseDouble(keyStats[StockUtility.KEY_STAT_PB_RATIO]);
			} catch (NumberFormatException e) {
				this.priceOverBook = -9999.0f;
			}
			try {// problem with not getting data for keyStats[3]
				if (keyStats[StockUtility.KEY_STAT_MARKET_CAP].contains("M")) {
					this.marketCap = Double
							.parseDouble(keyStats[StockUtility.KEY_STAT_MARKET_CAP]
									.replace("M", "\0")) * 1000000.0f;
				} else if (keyStats[StockUtility.KEY_STAT_MARKET_CAP]
						.contains("B")) {
					this.marketCap = Double
							.parseDouble(keyStats[StockUtility.KEY_STAT_MARKET_CAP]
									.replace("B", "\0")) * 1000000000.0f;
				} else {
					this.marketCap = -9999.0f;
				}
			} catch (NumberFormatException e) {
				this.marketCap = -9999.0f;
			}
			try {
				if (keyStats[StockUtility.KEY_STAT_REALTIME_MARKET_CAP]
						.contains("M")) {
					this.marketCapRealTime = Double
							.parseDouble(keyStats[StockUtility.KEY_STAT_REALTIME_MARKET_CAP]
									.replace("M", "\0")) * 1000000.0f;
				} else if (keyStats[StockUtility.KEY_STAT_REALTIME_MARKET_CAP]
						.contains("B")) {
					this.marketCapRealTime = Double
							.parseDouble(keyStats[StockUtility.KEY_STAT_REALTIME_MARKET_CAP]
									.replace("B", "\0")) * 1000000000.0f;
				} else {
					this.marketCapRealTime = -9999.0f;
				}
			} catch (NumberFormatException e) {
				this.marketCapRealTime = -9999.0f;
			}
			try {
				this.evOverEbitda = Double
						.parseDouble(keyStats[StockUtility.MISC_STAT_EV_INDEX]);
			} catch (NumberFormatException e) {
				this.evOverEbitda = 0;
			}

			try {
				this.priceOverFCF = this.marketCap
						/ ((Double
								.parseDouble(keyStats[StockUtility.MISC_STAT_FREE_CASH_FLOW_1]) + Double
								.parseDouble(keyStats[StockUtility.MISC_STAT_FREE_CASH_FLOW_2])) * 1000);
			} catch (NumberFormatException e) {
				this.priceOverFCF = 0;
			}
			if (this.marketCap != -9999.99) {/*
											 * System.out.println(symbol+" Div1:"
											 * + keyStats[StockUtility.
											 * MISC_STAT_DIVIDEND_1] + " Div2:"
											 * + keyStats[StockUtility.
											 * MISC_STAT_DIVIDEND_2] +
											 * " Buyback1:" +
											 * keyStats[StockUtility
											 * .MISC_STAT_BUYBACK_1] +
											 * " Buyback2:" +
											 * keyStats[StockUtility
											 * .MISC_STAT_BUYBACK_2]);
											 */
				try {
					this.shareholderYield = Double
							.parseDouble(keyStats[StockUtility.MISC_STAT_DIVIDEND_1]);
					this.shareholderYield += Double
							.parseDouble(keyStats[StockUtility.MISC_STAT_DIVIDEND_2]);
					this.shareholderYield -= Double
							.parseDouble(keyStats[StockUtility.MISC_STAT_BUYBACK_1]);
					this.shareholderYield -= Double
							.parseDouble(keyStats[StockUtility.MISC_STAT_BUYBACK_2]);
					this.shareholderYield *= 1000; // numbers were in thousands
					this.shareholderYield /= this.marketCap;
					this.shareholderYield *= 100; // multiple by 100 for
													// percentage
					/*
					 * System.out.println(symbol + " shareholder yield:" +
					 * this.shareholderYield);
					 */
				} catch (NumberFormatException e) {
					this.shareholderYield = 0;
					System.out.println(e.toString());
					System.out.println(symbol + " shareholder yield is 0");
				}
			}
		}

		// ignore stocks with really bad parameters
		if (this.priceOverEarnings > Filter.PE_LIMIT
				|| this.priceOverSales > Filter.PS_LIMIT
				|| this.priceOverBook > Filter.PB_LIMIT
				|| this.priceOverFCF > Filter.PFCF_LIMIT
				|| this.shareholderYield > Filter.SHAREHOLDER_YIELD_LIMIT
				|| this.priceOverEarnings < 0 || this.priceOverSales < 0
				|| this.priceOverBook < 0
				|| this.marketCap < Filter.MARKET_CAP_LIMIT) {
			this.stockSymbol = null;
			return;
		}

		// maintain the maximum price over earnings ratio seen
		if (maxPriceOverEarnings < this.priceOverEarnings) {
			maxPriceOverEarnings = this.priceOverEarnings;
		}

		// maintain price over sales ratio seen
		if (maxPriceOverSales < this.priceOverSales) {
			maxPriceOverSales = this.priceOverSales;
		}

		// maintain price over book ratio seen
		if (maxPriceOverBook < this.priceOverBook) {
			maxPriceOverBook = this.priceOverBook;
		}

		// maintain maxEvOverEbitda rank
		if (maxEvOverEbitda < this.evOverEbitda) {
			maxEvOverEbitda = this.evOverEbitda;
		}

		// maintain max Price over Free Cash flow
		if (maxPriceOverFCF < this.priceOverFCF) {
			maxPriceOverFCF = this.priceOverFCF;
			maxPriceOverFCFSymbol = symbol;
			System.out.println("maxPriceOverFCFSymbol is:"
					+ maxPriceOverFCFSymbol);
		}

		// maintain investor yield
		if (maxShareholderYield < this.shareholderYield) {
			maxShareholderYield = this.shareholderYield;
			Stock.maxShareholderYieldSymbol = symbol;
			System.out.println("current max yield is:" + maxShareholderYield
					+ " " + symbol);

		}

		if (dataEntries.size() != numValidTradingDays) {
			System.out.println("Number of entries for " + symbol
					+ " does not match the number of trading days");
		}

		int index = numValidTradingDays - 1;

		if (StockUtility.DEBUG_ENABLED == false) {
			// Loop through all rows in containing stock data
			for (String[] stringArray : dataEntries) {
				if (index < 0) {
					System.out.println("Stock:" + symbol
							+ " has invalid number of active days");
					break;
				}
				// fill in stock data
				try {
					// System.out.println("Index is " + index);

					dailyPrices[index] = Float
							.parseFloat((stringArray[StockUtility.CLOSE_PRICE_INDEX]));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				index--;

			}
		}
		// find the first entry that is not equal to -999.0f
		int t = 0;
		for (t = 0; t < numValidTradingDays; t++) {
			if (dailyPrices[t] != -999.0f) {
				break;
			}
		}

		this.sixMonthChange = dailyPrices[t]
				- dailyPrices[numValidTradingDays - 1];
		/*
		 * stop = System.currentTimeMillis(); total = stop - start;
		 * 
		 * System.out.println(String.format("Start:%d, Stop:%d, Total:%d",
		 * start, stop, total));
		 */
		this.valid = true;
	}

	/**
	 * initializes the set of Dates used to grab information on stocks,
	 * determines which days stocks were actively traded on based on S&P 500
	 */
	public static void initDates() {

		String request;

		Calendar cal = Calendar.getInstance();

		/*
		 * startDate = cal.getTime(); cal.set(Calendar.YEAR, stopYear);
		 * cal.set(Calendar.MONTH, 11); cal.set(Calendar.DAY_OF_MONTH, 31);
		 * stopDate = cal.getTime();
		 */
		stopDate = cal.getTime();
		cal.add(Calendar.MONTH, -6);
		startDate = cal.getTime();

		// get the days the S&P 500 was actively traded
		request = StockUtility.generateStockUrl(startDate, stopDate, "^GSPC");
		// System.out.println(request);

		List<String[]> myEntries = StockUtility.getStockEntries(request);

		numValidTradingDays = myEntries.size();
		dates = new Date[numValidTradingDays];

		int index = numValidTradingDays - 1;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (String[] stringArray : myEntries) {
			// fill in dates array
			try {
				dates[index] = dateFormat.parse(stringArray[0]);
			} catch (ParseException e) {
				System.out.println("Could not parse the Date");
				e.printStackTrace();
			}
			index--;
		}
	}

	/* price over earnings ratio */
	public double getPE() {
		return this.priceOverEarnings;
	}

	public void setPERank(double value) {
		this.priceOverEarningsRank = value;
	}

	public double getPERank() {
		return this.priceOverEarningsRank;
	}

	/* shareholder yield */
	public void setShareholderYieldRank(double value) {
		this.shareholderYieldRank = value;
	}

	public double getShareholderYield() {
		return this.shareholderYield;
	}

	public double getShareholderYieldRank() {
		return this.shareholderYieldRank;
	}

	/* price over shares ratio */
	public double getPS() {
		return this.priceOverSales;
	}

	public void setPSRank(double value) {
		this.priceOverSalesRank = value;
	}

	public double getPSRank() {
		return this.priceOverSalesRank;
	}

	/*
	 * earned value over Earning before interest, tax, depreciation,
	 * ammortization
	 */
	public double getEV() {
		return this.evOverEbitda;
	}

	public double getEVRank() {
		return this.evOverEbitdaRank;
	}

	public void setEVRank(double value) {
		this.evOverEbitdaRank = value;
	}

	/* price over free cash flow ratio */
	public double getPFCF() {
		return this.priceOverFCF;
	}

	public void setPFCFRank(double value) {
		this.priceOverFCFRank = value;
	}

	public double getPFCFRank() {
		return this.priceOverFCFRank;
	}

	/* price over bookings ratio */
	public double getPB() {
		return this.priceOverBook;
	}

	public void setPBRank(double value) {
		this.priceOverBookRank = value;
	}

	public double getPBRank() {
		return this.priceOverBookRank;
	}

	public void setTotalRank(double value) {
		this.totalRank = value;
	}
}
