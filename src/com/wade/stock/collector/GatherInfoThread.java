package com.wade.stock.collector;

import com.wade.stock.common.DataLoader;
import com.wade.stock.common.Stock;

/**
 * the purpose of this class is to create the thread that will gather data for a
 * stock
 * 
 * @author wadenakagawa
 * 
 */
public class GatherInfoThread implements Runnable {
	private String stockSymbol;
	private String sector;
	private boolean getFreshCopy;
	private static int stockThreadCount = 0;

	public void run() {
		try {
			Stock stock = new Stock(stockSymbol, getFreshCopy, this.sector);
			if (stock.valid == true) { // ignore invalid stocks
				DataLoader.addToStockList(stock);
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Thread exception for:" + stockSymbol);
		}

	}

	public GatherInfoThread(String symbol, boolean getFreshCopy, String sector) {
		this.stockSymbol = symbol;
		this.getFreshCopy = getFreshCopy;
		this.sector = sector;
		stockThreadCount++;
	}

	static public  void getUsedThreadCount() {
		System.out.println("Number of threads used: " + stockThreadCount);
	}

}
