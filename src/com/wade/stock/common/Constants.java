package com.wade.stock.common;

public class Constants {

	/**
	 * number of days in a year, not acounting for leap years
	 */
	static final public int DAYS_IN_A_YEAR = 365;
	
	/**
	 * number of weeks in a year
	 */
	static final public int WEEKS_IN_A_YEAR = 52;

	/**
	 * number of active trading days in a year, doesn not account for holidays!
	 */
	static final public int ACTIVE_TRADING_DAYS_IN_A_YEAR = DAYS_IN_A_YEAR
			- (2 * WEEKS_IN_A_YEAR);

}
