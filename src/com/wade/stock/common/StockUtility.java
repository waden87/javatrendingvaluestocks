package com.wade.stock.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.wade.stock.analyzer.OutputFormatter;

/**
 * 
 * @author wadenakagawa http://www.nasdaq.com/screening/company-list.aspx
 *         -contains list of stocks on NASDAQ and NYSE provides back common
 *         functions
 */
public class StockUtility {

	public static final int COMPANY_LIST_STOCK_SYMBOL_INDEX = 0; // symbol
	public static final int COMPANY_LIST_STOCK_NAME_INDEX = 1; // stock name
	public static final int COMPANY_LIST_STOCK_PRICE_INDEX = 2;// last price
	public static final int COMPANY_LIST_STOCK_CAP_INDEX = 3;// market cap size
	public static final int COMPANY_LIST_STOCK_ADR_TSO_INDEX = 4;// unused
	public static final int COMPANY_LIST_STOCK_IPO_YEAR_INDEX = 5;// IPO year
	public static final int COMPANY_LIST_STOCK_SECTOR_INDEX = 6;// sector
	public static final int COMPANY_LIST_STOCK_INDUSTRY_INDEX = 7;// industry
	public static final int COMPANY_LIST_STOCK_SUMMARY_INDEX = 8;// stock info

	/*
	 * Key stats array
	 */
	// r=P/E ratio
	// p5=P/Sales Ratio
	// p6=P/Booking Ratio
	// j1=marketcap j3=
	// marketcap(realtime)
	// price over earnings ratio
	public static final int KEY_STAT_PE_RATIO = 0;
	// price over sales ratio
	public static final int KEY_STAT_PS_RATIO = 1;
	// price over bookings ratio
	public static final int KEY_STAT_PB_RATIO = 2;
	// market cap
	public static final int KEY_STAT_MARKET_CAP = 3;
	// last quarter's free cash flow
	public static final int KEY_STAT_REALTIME_MARKET_CAP = 4;
	// Enterprise Value/EBITDA
	public static final int MISC_STAT_EV_INDEX = 5;
	// last quarter's free cash flow
	public static final int MISC_STAT_FREE_CASH_FLOW_1 = 6;
	// second to last quarters free cash flow
	public static final int MISC_STAT_FREE_CASH_FLOW_2 = 7;
	// last quarters dividend
	public static final int MISC_STAT_DIVIDEND_1 = 8;
	// second to last quarters dividend
	public static final int MISC_STAT_DIVIDEND_2 = 9;
	// last quarters sale/purchase of stock
	public static final int MISC_STAT_BUYBACK_1 = 10;
	// second to last quarters sale/purchase of stock
	public static final int MISC_STAT_BUYBACK_2 = 11;

	public static boolean DEBUG_ENABLED = false;
	/**
	 * specifies the column index (base 0) of the closing price in a csv file
	 */
	public final static int CLOSE_PRICE_INDEX = 3;

	/**
	 * generates the URL used to retrieve historical data for a particular stock
	 * 
	 * @param start
	 *            FIRST day of specified year
	 * @param stop
	 *            LAST day of specified year
	 * @param symbol
	 *            Stock string name
	 * @return URL to parse
	 */
	public static String generateStockUrl(Date start, Date stop, String symbol) {
		String request;
		StringBuilder builder = new StringBuilder();
		Calendar startDate = Calendar.getInstance();
		Calendar stopDate = Calendar.getInstance();

		startDate.setTime(start);
		stopDate.setTime(stop);

		// http://ichart.finance.yahoo.com/table.csv?s=YHOO&d=0&e=28&f=2010&g=d&a=3&b=12&c=1996&ignore=.csv
		/*
		 * sn = TICKER a = fromMonth-1 b = fromDay (two digits) c = fromYear d =
		 * toMonth-1 e = toDay (two digits) f = toYear g = d for day, m for
		 * month, y for yearly
		 */
		builder.append("http://ichart.finance.yahoo.com/table.csv?s=");
		builder.append(symbol); // use S&P 500 as starting point
		// specify stop month
		builder.append("&d=");
		builder.append(stopDate.get(Calendar.MONTH));// stop month (dec)

		// specify stop day of month
		builder.append("&e=");
		builder.append(stopDate.get(Calendar.DAY_OF_MONTH));// stop day of month
															// (last day in
															// december)

		// specify stop year
		builder.append("&f=");
		builder.append(stopDate.get(Calendar.YEAR));// stop year (same year)

		// specify that we want daily info
		builder.append("&g=d");

		// specify start month
		builder.append("&a=");// January 0
		builder.append(startDate.get(Calendar.MONTH));// January 0//start month

		// specify start day of month
		builder.append("&b=");// day 01
		builder.append(startDate.get(Calendar.DAY_OF_MONTH));// start day of
																// month

		// specify start year
		builder.append("&c=");// from year
		builder.append(startDate.get(Calendar.YEAR));// start year
		builder.append("&ignore=.csv");

		request = builder.toString();
		// System.out.println(request);
		return request;
	}

	/**
	 * stores the historical stock prices for a stock
	 * 
	 * @param symbol
	 *            stock id
	 * @param dataEntries
	 *            historical data entries
	 */
	public static void storeData(String symbol, List<String[]> dataEntries) {
		FileWriter writer = null;
		new File("./stored_data").mkdirs();
		try {
			writer = new FileWriter("./stored_data/" + symbol + ".csv");
			CSVWriter csvWriter = new CSVWriter(writer);
			csvWriter.writeAll(dataEntries);
			csvWriter.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * stores the key statistics associated with a stock
	 * 
	 * @param symbol
	 *            stock id
	 * @param keyStats
	 *            statistics
	 */
	public static void storeKeyStats(String symbol, String[] keyStats) {
		FileWriter writer = null;

		new File("./stored_data").mkdirs();
		try {
			writer = new FileWriter("./stored_data/" + symbol + "_keyStats.csv");
			CSVWriter csvWriter = new CSVWriter(writer);
			csvWriter.writeNext(keyStats);
			csvWriter.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static List<String[]> getStoredStockEntry(String symbol) {
		if (DEBUG_ENABLED == false) {
			FileReader fileReader;
			try {
				fileReader = new FileReader("./stored_data/" + symbol + ".csv");
			} catch (IOException e) {
				// e.printStackTrace();
				System.out.println("Could not find stored data for:" + symbol);
				return null;
			}
			CSVReader reader = new CSVReader(fileReader, ',', '"', 0);

			List<String[]> myEntries = new ArrayList<String[]>();
			try {
				myEntries = reader.readAll(); // entries are in reversed order
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					reader.close();
					fileReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return myEntries;
		} else {
			return new ArrayList<String[]>();
		}
	}

	public static String[] getStoredKeyStats(String symbol) {
		FileReader fileReader;
		try {
			fileReader = new FileReader("./stored_data/" + symbol
					+ "_keyStats.csv");

		} catch (FileNotFoundException e) {
			// e.printStackTrace();
			System.out.println("Cannot find stored key stats for:" + symbol);
			return null;
		}

		CSVReader reader = new CSVReader(fileReader, ',', '"', 0);

		String[] data = null;
		try {
			data = reader.readAll().get(0); // entries are in reversed order
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	/**
	 * grab the historical data for a stock from yahoo icharts
	 * 
	 * @param request
	 * @return null if data range in invalid for the stock
	 */
	public static List<String[]> getStockEntries(String request) {
		URL url = null;
		URLConnection openConnection = null;
		InputStream inputStream = null;
		try {
			url = new URL(request);
			openConnection = url.openConnection();
			openConnection.setReadTimeout(1000);
			inputStream = openConnection.getInputStream();
		} catch (IOException e) {
			System.out
					.println("ERROR: could not retrieve data for ("
							+ request
							+ ").  This could be do to the stock not existing, or the data for that time range in unavailable."
							+ e.toString());
			// e.printStackTrace();
			return null;
		}

		CSVReader reader = new CSVReader(new InputStreamReader(inputStream),
				',', '"', 0);

		List<String[]> myEntries = new ArrayList<String[]>();
		try {
			myEntries = reader.readAll(); // entries are in reversed order
			myEntries.remove(0);
		} catch (IOException e) {
			System.out
					.println("Error: trying to read in CSV file for historical data."
							+ e.toString());
		} finally {
			try {
				inputStream.close();
				reader.close();
			} catch (IOException e) {
				System.out.println("Error trying to close csv file."
						+ e.toString());
			}
		}
		return myEntries;
	}

	public static List<String[]> getStockTickers(String companyList) {
		FileReader inputStream = null;
		try {
			inputStream = new FileReader(companyList);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// BufferedInputStream bis = new BufferedInputStream(inputStream);

		CSVReader reader = new CSVReader(inputStream, ',', '"', 0);

		List<String[]> myEntries = new ArrayList<String[]>();
		try {
			myEntries = reader.readAll(); // entries are in reversed order
			myEntries.remove(0); // first row of head information

			if (DEBUG_ENABLED == true) {
				// REDUCE THE LIST to shorten gathering process time
				myEntries = myEntries.subList(0, 500);
			}
		} catch (IOException e) {
			System.out.println("Cannot read list of stocks");
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return myEntries;
	}

	/**
	 * gets the key statistics for a stock, this includes P/E, P/Sales,
	 * P/Booking, market capitolization, market cap realtime
	 * 
	 * @param symbol
	 * @return
	 */
	public static String[] getKeyStatistics(String symbol) {
		URL url;
		URLConnection openConnection;
		InputStream inputStream = null;
		String connectionUrl = null;
		try {
			/**
			 * r=P/E ratio p5=P/Sales Ratio p6=P/Booking Ratio j1=marketcap
			 * j3=marketcap(realtime)
			 */
			connectionUrl = new String(
					"http://finance.yahoo.com/d/quotes.csv?s=" + symbol
							+ "&f=rp5p6j1j3");
			url = new URL(connectionUrl);
			openConnection = url.openConnection();
			inputStream = openConnection.getInputStream();
		} catch (IOException e) {
			System.out.println("Problem getting Key Statistics for: " + symbol
					+ " " + connectionUrl + " " + e.toString());
			return null;
		}
		CSVReader reader = new CSVReader(new InputStreamReader(inputStream),
				',', '"', 0);

		String data[] = null;
		try {
			data = reader.readAll().get(0); // get only the first row

		} catch (IOException e) {
			System.out.println("Error reading in key statistics."
					+ e.toString());
		} finally {
			try {
				inputStream.close();
				reader.close();
			} catch (IOException e) {
				System.out.println("Error closing CSV file." + e.toString());
				e.printStackTrace();
			}
		}

		return data;
	}

	/**
	 * returns the EV/EBITDA, Free Cash Flow
	 * 
	 * @param symbol
	 * @return
	 */
	public static String[] getMiscStats(String symbol) {
		String value;
		String html;
		String[] freeCashFlow;

		try {
			html = Jsoup
					.connect(
							"http://finance.yahoo.com/q/ks?s=" + symbol
									+ "+Key+Statistics").get().html();
		} catch (IOException e) {
			System.out
					.println(symbol
							+ " could not establish connection to retrieve key statisctics");
			return null;
		}

		int evIndex = html.indexOf("Enterprise Value/EBITDA");
		if (evIndex == -1) {
			System.out.println(symbol + " missing EV/EBITDA");
			return null;
		} else {
			// find the ev section
			String startEv = html.substring(evIndex);
			int startOfEv = startEv.indexOf("<td ");

			// find the start of ev
			String evSection = startEv.substring(startOfEv);
			// System.out.println(evSection);
			int evValue = evSection.indexOf(">");

			// grab ev value;

			String valueStart = evSection.substring(evValue + 1);
			int evEnd = valueStart.indexOf("<");

			value = valueStart.substring(0, evEnd);

			// System.out.println("Value is" + value);
		}

		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
			System.out.println("error sleeping while retriving the cash flow"
					+ e.toString());
		}

		freeCashFlow = getFreeCashFlow(symbol);
		if (freeCashFlow != null) {
			String ret[] = { value, freeCashFlow[0], freeCashFlow[1],
					freeCashFlow[2], freeCashFlow[3], freeCashFlow[4],
					freeCashFlow[5] };
			return ret;
		}
		return null;
	}

	/**
	 * grabs the last and second to last quarter's free cash flow value in
	 * thousands. This is the "Total Cash Flow From Operating Activities" minus
	 * "Capital Expenditures" which can be found on the cash flow page
	 * 
	 * @param symbol
	 * @return
	 */
	private static String[] getFreeCashFlow(String symbol) {
		String value, value2;
		String[] values;
		String operatingIncome1, operatingIncome2, capEx1, capEx2;
		String html;
		try {
			html = Jsoup.connect("http://finance.yahoo.com/q/cf?s=" + symbol)
					.get().html();
		} catch (IOException e) {
			System.out
					.println("Could not establish connection to retrieve the quarterly cash flows");
			return null;
		}

		try {
			values = getShareholderYield(html);
		} catch (StringIndexOutOfBoundsException e) {
			System.out.println("Could not find shareholder yield for: "
					+ symbol);
			values = new String[] { "0", "0", "0", "0" };
		}
		// get the stock dividend and buyback amount to calculate shareholder
		// yield later
		/*
		 * example <strong> Total Cash Flow From Operating Activities </strong>
		 * </strong> </td><td align="right"> <strong> (33,269) </strong>
		 * </td><td align="right"> <strong> 28,762&nbsp;&nbsp; </strong>
		 */
		// search for this string
		int evIndex = html.indexOf("Total Cash Flow From Operating Activities");
		if (evIndex == -1) {
			return null;
		} else {
			// find last quarter free cash flow section
			String cashSection = html.substring(evIndex);

			// grab text after first <strong
			String cashFlowBeginStrong = cashSection.substring(cashSection
					.indexOf("<strong"));
			String cashFlowStart = cashFlowBeginStrong
					.substring(cashFlowBeginStrong.indexOf(">") + 1);

			operatingIncome1 = cashFlowStart.substring(0,
					cashFlowStart.indexOf("</strong>"));

			// grab the next operating income
			String fashFlow2Section = cashFlowStart.substring(cashFlowStart
					.indexOf("<strong"));
			String cashFlowStart2 = fashFlow2Section.substring(fashFlow2Section
					.indexOf(">") + 1);
			operatingIncome2 = cashFlowStart2.substring(0,
					cashFlowStart2.indexOf("</strong>"));

			// find the first capital expenditure section
			int capitalSectionStartIndex = cashFlowStart2
					.indexOf("Capital Expenditures");
			if (capitalSectionStartIndex != -1) {
				String capitalSectionStart = cashFlowStart2
						.substring(capitalSectionStartIndex);

				// find the td section start
				String tdSectionStart = capitalSectionStart
						.substring(capitalSectionStart.indexOf("<td"));
				String capBegin1 = tdSectionStart.substring(tdSectionStart
						.indexOf(">") + 1);

				capEx1 = capBegin1.substring(0, capBegin1.indexOf("</td>"));

				String tdSectionStart2 = capBegin1.substring(capBegin1
						.indexOf("<td"));
				String capBegin2 = tdSectionStart2.substring(tdSectionStart2
						.indexOf(">") + 1);

				capEx2 = capBegin2.substring(0, capBegin2.indexOf("</td>"));

				/* replace negative symbol with a zero */
				if (operatingIncome1.contains("-")) {
					operatingIncome1 = new String("0");
				}
				if (operatingIncome2.contains("-")) {
					operatingIncome2 = new String("0");
				}
				if (capEx1.contains("-")) {
					capEx1 = new String("0");
				}
				if (capEx2.contains("-")) {
					capEx2 = new String("0");
				}
				/*
				 * replace parathensis with a negative value, and remove the
				 * ampersand character
				 */

				if (operatingIncome1.contains("(")) {
					operatingIncome1 = operatingIncome1.replace("(", "-");
					operatingIncome1 = operatingIncome1.replace(")", "\0");
				}

				if (operatingIncome2.contains("(")) {
					operatingIncome2 = operatingIncome2.replace("(", "-");
					operatingIncome2 = operatingIncome2.replace(")", "\0");
				}

				if (capEx1.contains("(")) {
					capEx1 = capEx1.replace("(", "-");
					capEx1 = capEx1.replace(")", "\0");
				}

				if (capEx2.contains("(")) {
					capEx2 = capEx2.replace("(", "-");
					capEx2 = capEx2.replace(")", "\0");
				}

				/* remove the &nsbp */
				if (operatingIncome1.contains("&")) {
					operatingIncome1 = operatingIncome1.replace("&nbsp;", "\0");
				}

				if (operatingIncome2.contains("&")) {
					operatingIncome2 = operatingIncome2.replace("&nbsp;", "\0");
				}

				if (capEx1.contains("&")) {
					capEx1 = capEx1.replace("&nbsp;", "\0");
				}

				if (capEx2.contains("&")) {
					capEx2 = capEx2.replace("&nbsp;", "\0");
				}
				/* remove the comma with nothing */
				if (operatingIncome1.contains(",")) {
					operatingIncome1 = operatingIncome1.replace(",", "");
				}

				if (operatingIncome2.contains(",")) {
					operatingIncome2 = operatingIncome2.replace(",", "");
				}

				if (capEx1.contains(",")) {
					capEx1 = capEx1.replace(",", "");
				}

				if (capEx2.contains(",")) {
					capEx2 = capEx2.replace(",", "");
				}

				// remove any white space characters
				try {
					value = Double.toString(Double
							.parseDouble(operatingIncome1)
							+ Double.parseDouble(capEx1));
				} catch (NumberFormatException e) {
					value = new String("0");

				}

				try {
					value2 = Double.toString(Double
							.parseDouble(operatingIncome2)
							+ Double.parseDouble(capEx2));
				} catch (NumberFormatException e) {
					value2 = new String("0");
				}
			} else {
				// if we cant find the capital expenditure, set cash flow to 0
				// Log.getMainLogger().debug(
				// "Cannot find capital expenditure for:" + symbol);
				System.out.println("Cannot find capital expenditure for:"
						+ symbol);
				value = new String("0");
				value2 = new String("0");
			}
		}
		String ret[] = { value, value2, values[0], values[1], values[2],
				values[3] };
		return ret;
	}

	/**
	 * returns the shareholder yield which is comprised of the dividend and the
	 * sale/purchase of stocks
	 * 
	 * @param html
	 */
	public static String[] getShareholderYield(String html)
			throws StringIndexOutOfBoundsException {
		String dividend1 = null;
		String dividend2 = null;
		String buyback1 = null;
		String buyback2 = null;
		int div1StartIndex = html.indexOf("Dividends Paid");

		// find section where dividends paid is
		if (div1StartIndex == -1) {
			return new String[4];
		}
		String div1Start = html.substring(div1StartIndex);

		// grab the start of the first TD
		String div1Td = div1Start.substring(div1Start.indexOf("<td"));

		// grab beginning of first dividend
		String div1Begin = div1Td.substring(div1Td.indexOf(">") + 1);

		// find the end of the string
		dividend1 = div1Begin.substring(0, div1Begin.indexOf("</td>"));

		// find the beginning of second dividend
		String div2Td = div1Begin.substring(div1Begin.indexOf("<td"));

		// find the beginning of the dividend 2 string
		String div2Begin = div2Td.substring(div2Td.indexOf(">") + 1);

		// grab the actial dividend 2 here
		dividend2 = div2Begin.substring(0, div2Begin.indexOf("</td>"));

		// move to the beginning of the sale purchase of stock (stock buyback)
		String buybackSection = div2Begin.substring(div2Begin
				.indexOf("Sale Purchase of Stock"));

		// grab the beginning of the td for buyback 1
		String buyback1Td = buybackSection.substring(buybackSection
				.indexOf("<td"));

		// grab the beginning of the actual value for buyback 1
		String buyback1Begin = buyback1Td
				.substring(buyback1Td.indexOf(">") + 1);

		// grab the actual value for buyback 1
		buyback1 = buyback1Begin.substring(0, buyback1Begin.indexOf("</td>"));

		// grab the beginning of the td for backback 2
		String buyback2Td = buyback1Begin.substring(buyback1Begin
				.indexOf("<td"));

		// grab the beginning of the actual value for buyback 2
		String buyback2Begin = buyback2Td
				.substring(buyback2Td.indexOf(">") + 1);

		// grab the actual value of buyback 2
		buyback2 = buyback2Begin.substring(0, buyback2Begin.indexOf("</td>"));

		/* pre-process the strings */
		/* replace the negative symbol that is used to represent a null value */
		if (dividend1.contains("-")) {
			dividend1 = new String("0");
		}

		if (dividend2.contains("-")) {
			dividend2 = new String("0");
		}

		if (buyback1.contains("-")) {
			buyback1 = new String("0");
		}

		if (buyback2.contains("-")) {
			buyback2 = new String("0");
		}

		if (dividend1.contains("-")) {
			dividend1 = new String("0");
		}
		/* replace parenthesis with negative value */
		if (dividend1.contains("(")) {
			dividend1 = dividend1.replace("(", " ");
			dividend1 = dividend1.replace(")", "\0");
		}

		if (dividend2.contains("(")) {
			dividend2 = dividend2.replace("(", " ");
			dividend2 = dividend2.replace(")", "\0");
		}

		if (buyback1.contains("(")) {
			buyback1 = buyback1.replace("(", "-");
			buyback1 = buyback1.replace(")", "\0");
		}

		if (buyback2.contains("(")) {
			buyback2 = buyback2.replace("(", "-");
			buyback2 = buyback2.replace(")", "\0");
		}
		/* remove the &nsbp */
		if (dividend1.contains("&")) {
			dividend1 = dividend1.replace("&nbsp;", "\0");
		}

		if (dividend2.contains("&")) {
			dividend2 = dividend2.replace("&nbsp;", "\0");
		}

		if (buyback1.contains("&")) {
			buyback1 = buyback1.replace("&nbsp;", "\0");
		}

		if (buyback2.contains("&")) {
			buyback2 = buyback2.replace("&nbsp;", "\0");
		}
		/* remove the comma with nothing */
		if (dividend1.contains(",")) {
			dividend1 = dividend1.replace(",", "");
		}

		if (dividend2.contains(",")) {
			dividend2 = dividend2.replace(",", "");
		}

		if (buyback1.contains(",")) {
			buyback1 = buyback1.replace(",", "");
		}

		if (buyback2.contains(",")) {
			buyback2 = buyback2.replace(",", "");
		}
		/*
		 * System.out.println("DIV1:" + dividend1 + " DIV2:" + dividend2 +
		 * " BUYBACK1:" + buyback1 + " BUYBACK2:" + buyback2);
		 */
		String ret[] = { dividend1, dividend2, buyback1, buyback2 };
		return ret;
	}

	public static List<Stock> sortStockByRank(List<Stock> stockList) {
		LinkedList<Stock> newList = new LinkedList<Stock>();
		int count = stockList.size();
		for (int i = 0; i < count; i++) {
			int largestIndex = 0;
			double currentMax = 0.0;
			double currentValue = 0.0;
			int currentCount = stockList.size();
			for (int j = 0; j < currentCount; j++) {
				currentValue = stockList.get(j).totalRank;
				if (currentValue > currentMax) {
					currentMax = currentValue;
					largestIndex = j;
				}

			}
			newList.add(stockList.remove(largestIndex));
		}

		return newList;
	}

	public static void writeReport(List<Stock> stockList) {
		FileWriter writer = null;
		stockList = sortStockByRank(stockList);
		OutputFormatter outputFile = new OutputFormatter();
		try {
			writer = new FileWriter("reportStocks.csv");
			CSVWriter csvWriter = new CSVWriter(writer);
			StringBuilder headerBuilder = new StringBuilder();

			headerBuilder.append("Symbol,");
			headerBuilder.append("Sector,");
			headerBuilder.append("Total Rank,");
			headerBuilder.append("PE Rank,");
			headerBuilder.append("PS RANK,");
			headerBuilder.append("PB RANK,");
			headerBuilder.append("EV/EBITDA RANK,");
			headerBuilder.append("P/FCF RANK,");
			headerBuilder.append("Shareholder Yield RANK,");
			headerBuilder.append("Market Cap,");
			headerBuilder.append("Six-Month Change,");
			headerBuilder.append("Shareholder Yield,");
			headerBuilder.append("P/E,");
			headerBuilder.append("P/S,");
			headerBuilder.append("P/B,");
			headerBuilder.append("EV/EBITDA,");
			headerBuilder.append("P/FCF\n");
			/*
			 * String[] header = { "Symbol", "Total Rank", "PE Rank", "PS RANK",
			 * "PB RANK", "EV/EBITDA RANK", "P/FCF RANK",
			 * "Shareholder Yield RANK", "Market Cap", "Six-Month Change",
			 * "Shareholder Yield", "P/E", "P/S", "P/B", "EV/EBITDA", "P/FCF" };
			 */
			// csvWriter.writeNext(headerBuilder.toString().split(","));

			outputFile.writeOutputHeader(headerBuilder.toString().split(","));
			for (Stock stock : stockList) {
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append(stock.getSymbol() + ",");
				stringBuilder.append(stock.sector + ",");
				stringBuilder.append(stock.totalRank + ",");
				stringBuilder.append(stock.priceOverEarningsRank + ",");
				stringBuilder.append(stock.priceOverSalesRank + ",");
				stringBuilder.append(stock.priceOverBookRank + ",");
				stringBuilder.append(stock.evOverEbitdaRank + ",");
				stringBuilder.append(stock.priceOverFCFRank + ",");
				stringBuilder.append(stock.shareholderYieldRank + ",");
				stringBuilder.append(stock.marketCap + ",");
				stringBuilder.append(stock.sixMonthChange + ",");
				stringBuilder.append(stock.shareholderYield + ",");
				stringBuilder.append(stock.getPE() + ",");
				stringBuilder.append(stock.getPS() + ",");
				stringBuilder.append(stock.getPB() + ",");
				stringBuilder.append(stock.getEV() + ",");
				stringBuilder.append(stock.getPFCF() + ",");

				// String entry[] = stringBuilder.toString().split(",");
				// csvWriter.writeNext(entry);
				outputFile.writeOutputData(stringBuilder.toString().split(","));
			}
			outputFile.closeOutput();
			csvWriter.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
