package com.wade.stock.analyzer;

import java.io.FileWriter;
import java.io.IOException;

/**
 * the purpose of this class is to format the data that is output by the stock
 * analyzer into an html file that is easily viewable by the user
 * 
 * @author Wade
 * 
 */
public class OutputFormatter {
	private FileWriter writer;

	public OutputFormatter() {
		try {
			writer = new FileWriter("output.html");
			writer.append("<html>\n\t<body>");
		} catch (IOException e) {
			System.out.println("Error: could not create output file");
		}
	}

	public void writeOutputHeader(String[] header) {
		try {
			writer.append("\n<table border=1><tr>");

			for (String item : header) {
				writer.append("<td>" + item + "</td>");
			}
			writer.append("\n</tr>");
		} catch (IOException e) {
			System.out.println("Could not write header to output");
		}
	}
//FIXME append stocks sector in output data
	public void writeOutputData(String[] stockData) {
		try {
			writer.append("\n<tr>");
			int length = stockData.length;

			for (int i = 0; i < length; i++) {
				if (i == 0) {
					writer.append("<td><a href=\"http://finance.yahoo.com/q?s="
							+ stockData[0] + "\" target=\"_blank\">"
							+ stockData[0] + "</a></td>");
				} else {
					writer.append("<td>" + stockData[i] + "</td>");
				}
			}
			writer.append("\n</tr>");
		} catch (IOException e) {
			System.out.println("Could not write header to output");
		}

	}

	public void closeOutput() {
		try {
			writer.append("</table>\t</body>\n</html>");
			writer.close();
		} catch (IOException e) {
			System.out.println("Error, could not close the file");
		}
	}
}
