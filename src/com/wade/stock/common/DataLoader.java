package com.wade.stock.common;

import java.util.LinkedList;

import com.wade.stock.collector.GatherInfoThread;

/**
 * this class represents the database that holds all stocks found on the
 * internet with valid data for processing
 * 
 * @author wadenakagawa
 * 
 */
public class DataLoader {
	// lists all valid stocks in the dataloader
	public static LinkedList<Stock> stockList;

	// contains threads that are executing to collect stock data
	private LinkedList<Thread> collectorThreads;
	private ThreadGroup stockLoaderGroup;

	/**
	 * Retrieve list of all VALID stocks
	 * 
	 * @return stockList
	 */
	public LinkedList<Stock> getStockList() {
		return stockList;
	}

	public class StockNotFoundException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public StockNotFoundException(String message) {
			super(message);
		}
	}

	public DataLoader() {
		stockList = new LinkedList<Stock>();
		// allocate memory for threads
		collectorThreads = new LinkedList<Thread>();
		stockLoaderGroup = new ThreadGroup("collectorThreadGroup");
	}

	public void addStock(String symbol, boolean getFreshCopy, String sector) {
		GatherInfoThread infoThread = new GatherInfoThread(symbol, getFreshCopy, sector);
		Thread stockThread = new Thread(stockLoaderGroup, infoThread);
		// start the thread that will collect stock data
		collectorThreads.add(stockThread);
		stockThread.start();

		try {
			// prevent overwhelming yahoo stocks
			Thread.sleep(50L);
		} catch (InterruptedException e) {
			System.out.println("Problem with delay " + e.toString());
			e.printStackTrace();
		}
	}

	public static synchronized void addToStockList(Stock stock) {
		stockList.add(stock);
	}

	public Stock getStock(String symbol) throws StockNotFoundException {
		boolean found = false;
		Stock ret = null;
		for (Stock stock : stockList) {
			if (stock.getSymbol().equals(symbol)) {
				ret = stock;
				found = true;
				break;
			}
		}

		if (found == false) {
			throw new StockNotFoundException("Stock not found");
		}

		return ret;

	}

	public void waitForStockData() {
		// wait for all collector threads to finish gathering stock data
		/*
		 * for (Thread thread : collectorThreads) { try { thread.join(); } catch
		 * (InterruptedException e) {
		 * System.out.println("Error waiting for thread to terminate" +
		 * e.toString()); e.printStackTrace(); } }
		 */
		while (stockLoaderGroup.activeCount() != 0) {
			try {
				Thread.sleep(1000);
				System.out.println("Thread count:"
						+ stockLoaderGroup.activeCount());
			} catch (InterruptedException e) {
				System.out
						.println("Error sleeping while waiting for stock processing threads to complete");
				e.printStackTrace();
			}
		}
		stockLoaderGroup.destroy();
	}

}
