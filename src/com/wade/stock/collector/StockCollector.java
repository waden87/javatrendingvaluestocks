package com.wade.stock.collector;

import java.util.List;

import com.wade.stock.common.DataLoader;
import com.wade.stock.common.Stock;
import com.wade.stock.common.StockUtility;
import com.wade.stock.logger.Log;

public class StockCollector {

	public static void main(String[] args) {
		long start, stop, total;
		// record the start time for gathering stocks
		start = System.currentTimeMillis();
		// specify the range of dates to use for gathering data of stocks
		Stock.initDates();

		/*
		 * initialize the database that will hold all of the stock information
		 * that is found on the internet
		 */
		DataLoader loader = new DataLoader();

		/* obtain a list of all available stocks from the internet */
		List<String[]> tickers = StockUtility
				.getStockTickers("companylist.csv");

		List<String[]> tickers2 = StockUtility
				.getStockTickers("companylist2.csv");

		tickers.addAll(tickers2);

		if (tickers != null) {
			for (String[] ticker : tickers) {
				// don't use stock if sector is NA (may indicate index fund)
				if (ticker[StockUtility.COMPANY_LIST_STOCK_INDUSTRY_INDEX]
						.equals("n/a") == false) {
					loader.addStock(
							ticker[StockUtility.COMPANY_LIST_STOCK_SYMBOL_INDEX]
									.replace(" ", "\0"),
							true,
							ticker[StockUtility.COMPANY_LIST_STOCK_SECTOR_INDEX]);

				}
			}
		}

		// wait for data to be collected
		loader.waitForStockData();

		// record the stop time as the time we finished gathering information
		stop = System.currentTimeMillis();
		total = stop - start;
		System.out.println("Total time:" + total + "ms to process "
				+ loader.getStockList().size() + " stocks.");
		GatherInfoThread.getUsedThreadCount();
	}

}
