package com.wade.stock.analyzer;

import java.util.LinkedList;
import java.util.List;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import com.wade.stock.collector.GatherInfoThread;
import com.wade.stock.common.DataLoader;
import com.wade.stock.common.Stock;
import com.wade.stock.common.StockUtility;

public class StockAnalyzer extends Application {

	@Override
	public void start(Stage stage) {
		Filter filter = new Filter();
		Stock.initDates();
		DataLoader loader = new DataLoader();
		List<String[]> tickers = StockUtility
				.getStockTickers("companylist.csv");

		List<String[]> tickers2 = StockUtility
				.getStockTickers("companylist2.csv");

		tickers.addAll(tickers2);

		for (String[] ticker : tickers) {
			if (ticker[7].equals("n/a") == false) {
				// System.out.println("getting data for " + ticker[0]);
				loader.addStock(ticker[0], false,
						ticker[StockUtility.COMPANY_LIST_STOCK_SECTOR_INDEX]);
			}
		}

		LinkedList<Stock> stockList = loader.getStockList();

		stage.setTitle("Line Chart Sample");
		// defining the axes
		final CategoryAxis xAxis = new CategoryAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setLabel("Day of Year");
		// creating the chart
		final LineChart<String, Number> lineChart = new LineChart<String, Number>(
				xAxis, yAxis);

		lineChart.setTitle("Stock Monitoring");
		// defining a series
		XYChart.Series<String, Number> series = new XYChart.Series<String, Number>();
		series.setName("My portfolio - P/E");

		// determine ranks for stock paramters
		filter.applyRanks(stockList);
		// filter out stocks below a certain market capitolization
		stockList = filter.applyFilterTest(stockList);

		for (Stock stock : stockList) {
			series.getData().add(
					new XYChart.Data<String, Number>(stock.getSymbol(),
							stock.totalRank));
		}

		// export stock data that pass criteria to a csv file
		StockUtility.writeReport(stockList);

		// create a new scene and add the chart to it
		Scene scene = new Scene(lineChart, 800, 600);
		lineChart.getData().add(series);

		// stage the scene and show it
		stage.setScene(scene);
		stage.show();
		GatherInfoThread.getUsedThreadCount();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
